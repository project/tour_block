<?php

namespace Drupal\tour_block\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class TourBlockController.
 */
class TourBlockController extends ControllerBase {

  /**
   * Tour.
   *
   * @return string
   *   Return JSON tour data.
   */
  public function tour($tourId) {
    if (is_numeric($tourId) && $tourId > 0) {

      if ($tourId == 1) {
        $configuration_path = 'block.block.tourblock';
      }
      else {
        $configuration_path = 'block.block.tourblock_' . $tourId;
      }

      $settings = $this->config($configuration_path)->get('settings');

      if ($settings != NULL) {
        // Got data.
        $tour_data = [
          'field_tour_name' => $settings['field_tour_name'],
          'field_tour_stops' => $settings['field_tour_stops'],
        ];

        $response = new Response();
        $response->setContent(json_encode($tour_data));
        $response->headers->set('Content-Type', 'application/json');
        return $response;

      }

    }

    $response = new Response();
    $response->setContent(json_encode(['error' => 'Bad Tour ID']));
    $response->headers->set('Content-Type', 'application/json');
    return $response;

  }

}
