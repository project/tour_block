<?php

namespace Drupal\tour_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'TourBlock' block.
 *
 * @Block(
 *  id = "tour_block",
 *  admin_label = @Translation("Tour Block"),
 * )
 */
class TourBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'display_status' => 1,
      'hide_tour_when_completed' => 0,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['field_tour_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Tour Name'),
      '#description' => $this->t('The Name of this tour.'),
      '#default_value' => (isset($this->configuration['field_tour_name']) ? $this->configuration['field_tour_name'] : ''),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '10',
      '#required' => TRUE,
    ];

    $form['field_hide_tour_when_completed'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide Tour When Completed'),
      '#description' => $this->t('Hide the tour block when the tour has been completed.'),
      '#default_value' => (isset($this->configuration['field_hide_tour_when_completed']) ? $this->configuration['field_hide_tour_when_completed'] : FALSE),
      '#weight' => '20',
    ];

    $form['field_prerequisites'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Prerequisites'),
      '#description' => $this->t('Prerequisite tour names or cookie names separated by commas. If one or more prerequisites are specified the tour will be hidden unless cookies with those names exist. A series of tours can be specified in the same region and viewed one at a time.'),
      '#default_value' => (isset($this->configuration['field_prerequisites']) ? $this->configuration['field_prerequisites'] : ''),
      '#maxlength' => 128,
      '#size' => 64,
      '#weight' => '30',
    ];

    $form['field_keyboard_arrow_keys'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Keyboard Arrow Keys'),
      '#description' => $this->t('Activate Keyboard Back and Forward Keys.'),
      '#default_value' => (isset($this->configuration['field_keyboard_arrow_keys']) ? $this->configuration['field_keyboard_arrow_keys'] : TRUE),
      '#weight' => '40',
    ];

    $form['field_show_icons'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show Icons'),
      '#description' => $this->t('Show Icons as Navigation buttons. Bar and Square only.'),
      '#default_value' => (isset($this->configuration['field_show_icons']) ? $this->configuration['field_show_icons'] : TRUE),
      '#weight' => '50',
    ];

    $form['field_show_link_text'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show Link Text'),
      '#description' => $this->t('Show the link text for the tour link. Bar and Square only.'),
      '#default_value' => (isset($this->configuration['field_show_link_text']) ? $this->configuration['field_show_link_text'] : TRUE),
      '#weight' => '60',
    ];

    $form['field_show_status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show Status'),
      '#description' => $this->t('Wether to show the Status of the Tour. Example 1 / 42 for the first stop on a 442 step tour.  Bar and Square only.'),
      '#default_value' => (isset($this->configuration['field_show_status']) ? $this->configuration['field_show_status'] : TRUE),
      '#weight' => '70',
    ];

    $form['field_show_navigation_arrows'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show Navigation Arrows'),
      '#description' => $this->t('Show Navigation Arrows. Bar and Square only.'),
      '#default_value' => (isset($this->configuration['field_show_navigation_arrows']) ? $this->configuration['field_show_navigation_arrows'] : TRUE),
      '#weight' => '80',
    ];

    $form['field_previous_icon'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Previous Icon'),
      '#description' => $this->t('A link to the previous icon. Bar only.'),
      '#default_value' => (isset($this->configuration['field_previous_icon']) ? $this->configuration['field_previous_icon'] : 'scenic-drive-left-arrow-72x60.png'),
      '#maxlength' => 128,
      '#size' => 64,
      '#weight' => '90',
    ];

    $form['field_next_icon'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Next Icon'),
      '#description' => $this->t('Path to the Next Icon. Bar and Square only.'),
      '#default_value' => (isset($this->configuration['field_next_icon']) ? $this->configuration['field_next_icon'] : 'scenic-drive-right-arrow-72x60.png'),
      '#maxlength' => 128,
      '#size' => 64,
      '#weight' => '100',
    ];

    // Tour style popup.
    $form['field_tour_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Tour Style'),
      '#description' => $this->t('Choose a style of tour. Bar, Square or...'),
      '#default_value' => (isset($this->configuration['field_tour_style']) ? $this->configuration['field_tour_style'] : 'bar'),
      '#weight' => '120',
      '#options' => [
        'bar' => $this->t('Bar'),
        'square' => $this->t('Square'),
        'cta' => $this->t('CTA'),
        'cta-2' => $this->t('CTA 2'),
        'panel' => $this->t('Panel'),
        'card' => $this->t('Card'),
        'hero' => $this->t('Hero'),
        'jumbotron' => $this->t('Jumbotron'),
        'jumbotron-fluid' => $this->t('Fluid Jumbotron'),
      ],
    ];

    // For field_tour_stops.
    // The table of Links.
    // Problems in the header will break dnd.
    $form['table-row'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Tour&nbsp;Stops'),
        '',
        '',
        '',
        '',
        $this->t('Weight'),
      ],
      '#empty' => $this->t('Sorry, There are no items!'),
      // TableDrag: Each array value is a list of callback arguments for/.
      // Drupal_add_tabledrag(). The #id of the table is automatically.
      // Prepended; if there is none, an HTML ID is auto-generated.
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'table-sort-weight',
        ],
      ],
      '#weight' => '130',
    ];

    // Get $num_links.
    $num_links = $form_state->get('num_links');

    // We have to ensure that there is at least one link field.
    if ($num_links === NULL) {
      if (array_key_exists('field_tour_stops', $this->configuration)) {
        // There is a set of tour stops in the blocks EXISTING config.
        $tour_stops = $this->configuration['field_tour_stops'];
        if (is_array($tour_stops)) {
          $form_state->set('num_links', count($tour_stops));
          $num_links = count($tour_stops);
        }
      }
      else {
        // There were no PREVIOUSLY saved tour stops in the default config.
        $form_state->set('num_links', 1);
        $num_links = 1;
      }
    }

    // Iterate thru the Links.
    for ($i = 0; $i < $num_links; $i++) {
      // A row in the table.
      // TableDrag: Mark the table row as draggable.
      $form['table-row'][$i]['#attributes']['class'][] = 'draggable';
      // TableDrag: Sort the table row according to its existing/configured
      // weight.
      $form['table-row'][$i]['name'] = [
        '#markup' => 'Stop ' . ($i + 1),
      ];

      $form['table-row'][$i]['url'] = [
        '#type' => 'textfield',
        '#title' => $this->t('URL'),
        '#required' => TRUE,
        '#default_value' => (isset($tour_stops[$i]['url']) ? $tour_stops[$i]['url'] : ''),
        '#size' => 120,
        '#maxlength' => 512,
      ];

      $form['table-row'][$i]['link_title'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Link Title'),
        '#required' => FALSE,
        '#default_value' => (isset($tour_stops[$i]['link_title']) ? $tour_stops[$i]['link_title'] : ''),
        '#size' => 120,
        '#maxlength' => 255,
      ];

      $form['table-row'][$i]['title'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Title'),
        '#required' => FALSE,
        '#default_value' => (isset($tour_stops[$i]['title']) ? $tour_stops[$i]['title'] : ''),
        '#size' => 120,
        '#maxlength' => 255,
      ];

      $form['table-row'][$i]['description'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Description'),
        '#required' => FALSE,
        '#default_value' => (isset($tour_stops[$i]['description']) ? $tour_stops[$i]['description'] : ''),
        '#size' => 120,
        '#maxlength' => 512,
      ];

      // TableDrag: Weight column element.
      $form['table-row'][$i]['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight for @title', ['@title' => 'name']),
        '#title_display' => 'invisible',
        '#default_value' => $i,
        // Classify the weight element for #tabledrag.
        '#attributes' => ['class' => ['table-sort-weight']],
      ];

    }
    // End Of each row.
    $form['#tree'] = TRUE;

    // The Add and Remove Buttons in their Fieldset.
    $form['links_fieldset'] = [
      '#type' => 'fieldset',
      '#weight' => '200',
    ];

    $form['links_fieldset']['actions'] = [
      '#type' => 'actions',
    ];

    $form['links_fieldset']['actions']['add_tour_point'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add one more'),
      '#submit' => [[$this, 'addTourPoint']] ,
    ];

    // If there is more than one link, add the remove button.
    if ($num_links > 1) {
      $form['links_fieldset']['actions']['remove_tour_point'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove one'),
        '#submit' => [[$this, 'removeTourPoint']],
      ];
    }

    $form['#attached']['library'][] = 'tour_block/tour_block_widget';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    $this->configuration['field_tour_name'] = $form_state->getValue('field_tour_name');
    $this->configuration['field_hide_tour_when_completed'] = $form_state->getValue('field_hide_tour_when_completed');
    $this->configuration['field_prerequisites'] = $form_state->getValue('field_prerequisites');
    $this->configuration['field_keyboard_arrow_keys'] = $form_state->getValue('field_keyboard_arrow_keys');
    $this->configuration['field_show_icons'] = $form_state->getValue('field_show_icons');
    $this->configuration['field_show_link_text'] = $form_state->getValue('field_show_link_text');
    $this->configuration['field_show_status'] = $form_state->getValue('field_show_status');
    $this->configuration['field_show_navigation_arrows'] = $form_state->getValue('field_show_navigation_arrows');
    $this->configuration['field_tour_style'] = $form_state->getValue('field_tour_style');
    $this->configuration['tour_block_id'] = $this->getNumericFormId($form_state);

    // See if file names or urls were specified.
    $images_path = '/' . drupal_get_path('module', 'tour_block') . '/images/';

    $previous_icon_field_value = ($form_state->getValue('field_previous_icon') ?? '');
    if (substr($previous_icon_field_value, 0, 1) == '/' || strncmp($previous_icon_field_value, 'http', 4) == 0) {
      $this->configuration['field_previous_icon'] = $form_state->getValue('field_previous_icon');
    }
    else {
      $this->configuration['field_previous_icon'] = $images_path . $previous_icon_field_value;
    }

    $next_icon_field_value = ($form_state->getValue('field_next_icon') ?? '');
    if (substr($next_icon_field_value, 0, 1) == '/' || strncmp($next_icon_field_value, 'http', 4) == 0) {
      $this->configuration['field_next_icon'] = $form_state->getValue('field_next_icon');
    }
    else {
      $this->configuration['field_next_icon'] = $images_path . $next_icon_field_value;
    }

    // If you remove the the url and save the item(s) are removed.
    $old_tour_stops = $form_state->getValue('table-row');
    $cleaned_tour_stops = [];
    if (is_array($old_tour_stops) && count($old_tour_stops)) {
      foreach ($form_state->getValue('table-row') as $id => $item) {
        if (!empty($item['url'])) {
          $cleaned_tour_stops[] = $item;
        }
      }

      $this->configuration['field_tour_stops'] = $cleaned_tour_stops;

    }

  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['#theme'] = 'tour_block';
    $build['tour_block']['field_tour_name'] = $this->configuration['field_tour_name'];
    $build['tour_block']['field_hide_tour_when_completed'] = $this->configuration['field_hide_tour_when_completed'];
    $build['tour_block']['field_prerequisites'] = $this->configuration['field_prerequisites'];
    $build['tour_block']['field_keyboard_arrow_keys'] = $this->configuration['field_keyboard_arrow_keys'];
    $build['tour_block']['field_show_icons'] = $this->configuration['field_show_icons'];
    $build['tour_block']['field_show_link_text'] = $this->configuration['field_show_link_text'];
    $build['tour_block']['field_show_status'] = $this->configuration['field_show_status'];
    $build['tour_block']['field_show_navigation_arrows'] = $this->configuration['field_show_navigation_arrows'];
    $build['tour_block']['field_previous_icon'] = $this->configuration['field_previous_icon'];
    $build['tour_block']['field_next_icon'] = $this->configuration['field_next_icon'];
    $build['tour_block']['field_tour_style'] = $this->configuration['field_tour_style'];
    $build['tour_block']['tour_block_id'] = $this->configuration['tour_block_id'];

    $build['tour_block']['field_tour_stops'] = $this->configuration['field_tour_stops'];

    $build['#attached'] = [
      'library' => [
        'tour_block/tour_block',
      ],
    ];

    if ($this->tourIsBootstrapBaseTheme() == FALSE) {
      $build['#attached']['library'][] = 'tour_block/tour_block_no_bootstrap';
    }

    return $build;
  }

  /**
   * Implements submit callback for the Add Tour Stop button.
   *
   * @param array $form
   *   Form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current state of the form.
   */
  public function addTourPoint(array &$form, FormStateInterface $form_state) {
    // Increment num_links.
    $num_links = $form_state->get('num_links');

    $updated_num_links = $num_links + 1;
    $form_state->set('num_links', $updated_num_links);

    $form_state->setRebuild(TRUE);
  }

  /**
   * Implements submit callback for the Remove Tour Stop button.
   *
   * @param array $form
   *   Form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current state of the form.
   */
  public function removeTourPoint(array &$form, FormStateInterface $form_state) {
    // Decrement num_links.
    $num_links = $form_state->get('num_links');

    if ($num_links > 1) {
      $updated_num_links = $num_links - 1;
      $form_state->set('num_links', $updated_num_links);
    }

    $form_state->setRebuild(TRUE);
  }

  /**
   * Gets the numeric form id for this form.
   */
  public function getNumericFormId(FormStateInterface $form_state) {

    $numeric_form_id = $form_state->getformObject()->getEntity()->id();
    if (strcmp($numeric_form_id, 'tourblock') == 0) {
      return 1;
    }
    elseif (strncmp($numeric_form_id, 'tourblock_', strlen('tourblock_')) == 0) {
      $id_part = substr($numeric_form_id, strlen('tourblock_'));
      if (is_numeric($id_part)) {
        return $id_part;
      }

    }

    return 1;
  }

  /**
   * Indicates if the base theme is bootstrap or a subtheme of it.
   */
  public function tourIsBootstrapBaseTheme() {
    $system_list = system_list('theme');
    if (is_array($system_list) && array_key_exists('bootstrap', $system_list)) {
      return TRUE;
    }

    return FALSE;
  }

}
