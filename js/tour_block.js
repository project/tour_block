/**
 * @file
 * tour.js
 */

(function ($, Drupal) {
  var initialized

  function init () {
    if (!initialized) {
      initialized = true;
      // one-time only code here
      console.log('INIT');

      $('div.tour_block').each(function () {
        tourBlock(this);
      })

      $(document).keyup(function (e) {
        if (e.keyCode === 37 || e.keyCode === 39) {
          const elements = $('div.tour_block').not('.tour-hidden');
          let selector = '';
          if (elements.length === 1) {
            if (e.keyCode === 37) {
              selector = '#' + $(elements).prop('id') + ' a.previous-stop';
            } else if (e.keyCode === 39) {
              selector = '#' + $(elements).prop('id') + ' a.next-stop';
            }

            const tourName = $(selector).attr('tour-name');
            const tourStop = $(selector).attr('tour-stop');
            const arrowKeys = $('#' + $(elements).prop('id')).attr('field_keyboard_arrow_keys');

            if (arrowKeys) {
              $.cookie(tourName, tourStop, { path: '/', expires: 365 });

              if ($(selector).get(0).hasAttribute('last-stop')) {
                const lastStop = $(selector).attr('last-stop');
                $.cookie(tourName + '_completed', lastStop, { path: '/', expires: 365 });
              }

              const href = $(selector).prop('href');
              window.location = href;
            }

          }
        }
      })

      $('a.next-stop').click(function () {
        const tourName = $(this).attr('tour-name');
        const tourStop = $(this).attr('tour-stop');
        $.cookie(tourName, tourStop, { path: '/', expires: 365 });

        if ($(this).get(0).hasAttribute('last-stop')) {
          const lastStop = $(this).attr('last-stop');
          $.cookie(tourName + '_completed', lastStop, { path: '/', expires: 365 });
        }
      })

      $('a.previous-stop').click(function () {
        const tourName = $(this).attr('tour-name');
        //        let tourStop = $(this).attr('tour-stop') - 1;
        const tourStop = $(this).attr('tour-stop');
        $.cookie(tourName, tourStop, { path: '/', expires: 365 });
      })
    }
  }

  Drupal.behaviors.tour = {
    attach: function () {
      init();
    }
  }

  // For each tour block.
  function tourBlock (element) {
    const initialTourStop = 0;
    const tourIncrement = 1;
    let previousLinkIndex = initialTourStop;
    let nextLinkIndex = initialTourStop;
    const tourName = $(element).attr('field_tour_name');
    const prerequisites = $(element).attr('field_prerequisites');
    const hideTourWhenCompleted = $(element).attr('field_hide_tour_when_completed');
    const keyboardArrowKeys = $(element).attr('field_keyboard_arrow_keys');
    const blockId = $(element).attr('tour_block_id');
    const tourUri = '/tour/' + blockId;
    let lastStop = false;

    if (prerequisitesSatisfied(tourName, prerequisites) == false || tourShouldBeShown(tourName, hideTourWhenCompleted)  == false) {
      $('#' + tourName).addClass('tour-hidden');
      return
    }

    let tourCookieStop = $.cookie(tourName)
    if (typeof (tourCookieStop) === 'undefined') {
      // The tour cookie is not defined. It's the first stop.
      tourCookieStop = 0;
    }

    $.getJSON(tourUri, function (result) {

      if (result.error == 'Bad Tour ID') {
        console.log('Bad Tour ID');
      }

      if (tourCookieStop === initialTourStop) {
        // First stop.
        previousLinkIndex = Number(result.field_tour_stops.length) - Number(tourIncrement);
        nextLinkIndex = tourCookieStop;
      } else if (tourCookieStop >= result.field_tour_stops.length) {
        // Last stop.
        previousLinkIndex = tourCookieStop - Number(tourIncrement);
        nextLinkIndex = initialTourStop;
      } else {
        // In the middle.
        previousLinkIndex = tourCookieStop - Number(tourIncrement);
        nextLinkIndex = Number(tourCookieStop);// + Number(tourIncrement);
        if (tourCookieStop >= result.field_tour_stops.length - Number(tourIncrement)) {
          lastStop = true;
        }
      }



      // Previous.
      $('#' + tourName + ' a.previous-stop').attr('href', result.field_tour_stops[previousLinkIndex].url);
      $('#' + tourName + ' a.previous-stop').attr('tour-name', tourName);// not needed cause you dont update cookies
      $('#' + tourName + ' a.previous-stop').attr('tour-stop', previousLinkIndex + Number(tourIncrement));
      if (result.field_tour_stops[previousLinkIndex].description) {
        $('#' + tourName + ' a.previous-stop').attr('title', result.field_tour_stops[previousLinkIndex].description);
      } else {
        $('#' + tourName + ' a.previous-stop').attr('title', result.field_tour_stops[previousLinkIndex].title);
      }
      $('#' + tourName + ' a.previous-stop.link-text').text(result.field_tour_stops[previousLinkIndex].link_title);

      // Next.
      $('#' + tourName + ' a.next-stop').attr('href', result.field_tour_stops[nextLinkIndex].url);
      $('#' + tourName + ' a.next-stop').attr('tour-name', tourName);
      $('#' + tourName + ' a.next-stop').attr('tour-stop', nextLinkIndex + Number(tourIncrement));
      if (result.field_tour_stops[nextLinkIndex].description) {
        $('#' + tourName + ' a.next-stop').attr('title', result.field_tour_stops[nextLinkIndex].description);
      } else {
        $('#' + tourName + ' a.next-stop').attr('title', result.field_tour_stops[nextLinkIndex].title);
      }
      $('#' + tourName + ' a.next-stop.link-text').text(result.field_tour_stops[nextLinkIndex].link_title);

      if (lastStop) {
        $('#' + tourName + ' a.next-stop').attr('last-stop', 1);
      }

      // Status stop / total
      const status = tourCookieStop + '/' + result.field_tour_stops.length;
      $('#' + tourName + ' td.status-cell').text(status);

      // Description and title.
      $('#' + tourName + ' .tour-title').text(result.field_tour_stops[nextLinkIndex].title);
      $('#' + tourName + ' .tour-description').text(result.field_tour_stops[nextLinkIndex].description);
      $('#' + tourName).addClass('tour-stop-' + (previousLinkIndex + Number(tourIncrement)));

    });
  }

  function prerequisitesSatisfied (tourName, prerequisites) {
    let returnValue = true;
    if (prerequisites.length > 0) {
      if (prerequisites.indexOf(',') > 0) {
        // there are more than one requirements.
        const prerequisitesAray = CSVtoArray(prerequisites);
        prerequisitesAray.forEach(function (cookieName) {
          const namedCookie = $.cookie(cookieName);
          if (typeof (namedCookie) === 'undefined') {
            returnValue = false;
          }
        })
      } else {
        // there is only one requirment.
        const namedCookie = $.cookie(prerequisites);
        if (typeof (namedCookie) === 'undefined') {
          returnValue = false;
        }
      }
    }

    return returnValue;
  }

  function tourShouldBeShown(tourName, hideTourWhenCompleted) {
    if (hideTourWhenCompleted == 1) {
      const tourCookieCompleted = $.cookie(tourName + '_completed');
      if (typeof (tourCookieCompleted) === 'undefined') {
        // The tour cookie is not defined. It's the first stop.

        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  function CSVtoArray(text) {
    let ret = [''], i = 0, p = '', s = true;
    for (let l in text) {
      l = text[l];
      if ('"' === l) {
        s = !s;
        if ('"' === p) {
          ret[i] += '"';
          l = '-';
        } else if ('' === p)
          l = '-';
        } else if (s && ',' === l)
          l = ret[++i] = '';
        else
          ret[i] += l;
        p = l;
    }
    return ret;
  }

// ----------------------------------------------------------------------------------------------
}(jQuery, Drupal))
