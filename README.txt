CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers
 * Appendix - CSS



INTRODUCTION
------------

Tours are navigation blocks that allows users to navigate the site visiting pages in a specific order.

Tours can be hidden after they are completed.

Tours can be hidden and only shown if a prerequisite cookie with a the specified name exists.

Tours can be displayed in 9 Styles.

    Bar
    Square
    CTA 1
    CTA 2
    Panel
    Card
    Hero
    Jumbotron
    Fluid Jumbotron

 
Tours Blocks are static HTML so there are no special cacheing issues.
Tour Stops get populated by javascript.

Tour Stops are managed on the Block Configure page.

Tour Stops can be re-ordered by Drag and Drop.



 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/tour_block

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/tour_block


REQUIREMENTS
------------
There are no special requirements for using the Tour Block module.

 * Drupal 8.x

 * Works with Bootstrap or any Theme.



INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. 
 
 * If your using the Bootstrap theme there is nothing special to do.

 * If You have the Bootstrap theme but are not using it, ensure that Bootstrap and its  subthemes are Uninstalled. They can be on your system, just uninstalled. This allows the tour block module to use the correct css style for your theme.


CONFIGURATION
-------------
To create a new Tour Block go to your block layout page /admin/structure/block.

Click the Place Block Button for the region where you want to place the Tour Block.

When the Place Block screen appears choose the Tour Block by clicking the Place block button for Tour Block.

Give the tour a name.

Enter one or more Tour Stops.

Each Tour Stop has fields for:

URL:         The url on you site where the tour will stop.
             Used in all Tour Styles for the link or button..

Link Title:  The text title for the link.
             Used in all Tour Styles as the title of the link or button.

Title:       Used in all Tour Styles except Bar and Square.
             This is the title text.

Description: A longer text description of the tour stop.
             On Bar and Square this is what you see when you hover over a link.
             On the other Tour Styles this is the body text.


Use the Add one more button to add as many tour stops as necessary.

Use Drag and Drop to drag the Stops around by the + icon, or use row weights to sort.

Choose a page region to display the block in like any other block.

Click the Save block button.


TROUBLESHOOTING
---------------

Clear your caches.
/admin/config/development/performance

Check your page source to see if the modules css and javascript files are being loaded.

tour_block.css              Loaded on pages with tour blocks on them.
tour_block.js               Loaded on pages with tour blocks on them.
tour_block_no_bootstrap.css Loaded on pages with tour blocks on them if you are not using bootstrap.
tour_block_widget.css       Only used on Block Edit Pages.

You may need to disable Aggregation of CSS and Javascript files 
to see all the individual css and javascript files that are loaded.
You can do this Under bandwidth optimization a this page.
/admin/config/development/performance
Disable Sggregation and click Save configuration then click Clear all caches.


FAQ
---

Q: Can I add an image to Cards?

A: Yes, you can add background images to any style tour using css.

Q: Can I add different images for each tour stop?

A: Yes, you can add different background images or colors for every stop using css.


MAINTAINERS
-----------

Current maintainer:

 * Seth Snyder (tfa) - https://www.drupal.org/u/seth-snyder


APPENDIX - CSS
--------------


/*
 * You can add background images to any tour_block.
 *
 */

div.tour_block .cta,
div.tour_block .panel panel-body,
div.tour_block .hero-unit,
div.tour_block .jumbotron
{
  height: 100px;
  background-image: url("../images/user.svg");
  background-repeat: no-repeat;
  background-size: contain;
}


/*
 * for every and any type but different tour stops.
 *
 * The tour-stop class is numbered for each tour stop
 * .tour-stop-1 is for stop 1  .tour-stop-2 is for stop 2 etc.
 *
 */

div.tour_block.tour-stop-1 .cta {background-image: url("../images/clock.svg"); height: 100px; background-repeat: no-repeat; background-size: contain;}
div.tour_block.tour-stop-2 .cta {background-image: url("../images/dot-circle.svg"); height: 100px; background-repeat: no-repeat; background-size: contain;}
div.tour_block.tour-stop-3 .cta {background-image: url("../images/plus.svg") !important; height: 100px !important; background-repeat: no-repeat !important; background-size: contain !important;}
div.tour_block.tour-stop-4 .cta {background-image: url("../images/peace.svg"); height: 100px; background-repeat: no-repeat; background-size: contain;}
div.tour_block.tour-stop-5 .cta {background-image: url("../images/laugh.svg"); height: 100px; background-repeat: no-repeat; background-size: contain;}

div.tour_block.tour-stop-1 .cta-2 {background-image: url("../images/clock.svg"); height: 100px; background-repeat: no-repeat; background-size: contain;}
div.tour_block.tour-stop-2 .cta-2 {background-image: url("../images/dot-circle.svg"); height: 100px; background-repeat: no-repeat; background-size: contain;}
div.tour_block.tour-stop-3 .cta-2 {background-image: url("../images/plus.svg") !important; height: 100px !important; background-repeat: no-repeat !important; background-size: contain !important;}
div.tour_block.tour-stop-4 .cta-2 {background-image: url("../images/peace.svg"); height: 100px; background-repeat: no-repeat; background-size: contain;}
div.tour_block.tour-stop-5 .cta-2 {background-image: url("../images/laugh.svg"); height: 100px; background-repeat: no-repeat; background-size: contain;}

div.tour_block.tour-stop-1 .panel .panel-body {background-image: url("../images/clock.svg"); height: 100px; background-repeat: no-repeat; background-size: contain;}
div.tour_block.tour-stop-2 .panel .panel-body{background-image: url("../images/dot-circle.svg"); height: 100px; background-repeat: no-repeat; background-size: contain;}
div.tour_block.tour-stop-3 .panel .panel-body{background-image: url("../images/plus.svg") !important; height: 100px !important; background-repeat: no-repeat !important; background-size: contain !important;}
div.tour_block.tour-stop-4 .panel .panel-body{background-image: url("../images/peace.svg"); height: 100px; background-repeat: no-repeat; background-size: contain;}
div.tour_block.tour-stop-5 .panel .panel-body{background-image: url("../images/laugh.svg"); height: 100px; background-repeat: no-repeat; background-size: contain;}

div.tour_block.tour-stop-1 .card .card-img-top {background-image: url("../images/clock.svg"); height: 100px; background-repeat: no-repeat; background-size: contain;}
div.tour_block.tour-stop-2 .card .card-img-top {background-image: url("../images/dot-circle.svg"); height: 100px; background-repeat: no-repeat; background-size: contain;}
div.tour_block.tour-stop-3 .card .card-img-top {background-image: url("../images/plus.svg") !important; height: 100px !important; background-repeat: no-repeat !important; background-size: contain !important;}
div.tour_block.tour-stop-4 .card .card-img-top {background-image: url("../images/peace.svg"); height: 100px; background-repeat: no-repeat; background-size: contain;}
div.tour_block.tour-stop-5 .card .card-img-top {background-image: url("../images/laugh.svg"); height: 100px; background-repeat: no-repeat; background-size: contain;}

div.tour_block.tour-stop-1 .hero-unit {background-image: url("../images/clock.svg"); height: 100px; background-repeat: no-repeat; background-size: contain;}
div.tour_block.tour-stop-2 .hero-unit {background-image: url("../images/dot-circle.svg"); height: 100px; background-repeat: no-repeat; background-size: contain;}
div.tour_block.tour-stop-3 .hero-unit {background-image: url("../images/plus.svg") !important; height: 100px !important; background-repeat: no-repeat !important; background-size: contain !important;}
div.tour_block.tour-stop-4 .hero-unit {background-image: url("../images/peace.svg"); height: 100px; background-repeat: no-repeat; background-size: contain;}
div.tour_block.tour-stop-5 .hero-unit {background-image: url("../images/laugh.svg"); height: 100px; background-repeat: no-repeat; background-size: contain;}

div.tour_block.tour-stop-1 .jumbotron {background-image: url("../images/clock.svg"); height: 100px; background-repeat: no-repeat; background-size: contain;}
div.tour_block.tour-stop-2 .jumbotron {background-image: url("../images/dot-circle.svg"); height: 100px; background-repeat: no-repeat; background-size: contain;}
div.tour_block.tour-stop-3 .jumbotron {background-image: url("../images/plus.svg") !important; height: 100px !important; background-repeat: no-repeat !important; background-size: contain !important;}
div.tour_block.tour-stop-4 .jumbotron {background-image: url("../images/peace.svg"); height: 100px; background-repeat: no-repeat; background-size: contain;}
div.tour_block.tour-stop-5 .jumbotron {background-image: url("../images/laugh.svg"); height: 100px; background-repeat: no-repeat; background-size: contain;}

div.tour_block.tour-stop-1 .jumbotron.jumbotron-fluid {background-image: url("../images/clock.svg"); height: 100px; background-repeat: no-repeat; background-size: contain;}
div.tour_block.tour-stop-2 .jumbotron.jumbotron-fluid {background-image: url("../images/dot-circle.svg"); height: 100px; background-repeat: no-repeat; background-size: contain;}
div.tour_block.tour-stop-3 .jumbotron.jumbotron-fluid {background-image: url("../images/plus.svg") !important; height: 100px !important; background-repeat: no-repeat !important; background-size: contain !important;}
div.tour_block.tour-stop-4 .jumbotron.jumbotron-fluid {background-image: url("../images/peace.svg"); height: 100px; background-repeat: no-repeat; background-size: contain;}
div.tour_block.tour-stop-5 .jumbotron.jumbotron-fluid {background-image: url("../images/laugh.svg"); height: 100px; background-repeat: no-repeat; background-size: contain;}


/*
 * Most styles have an .img-top and .img-bottom elements
 *
 * You can add background images to those elements easily..
 *
 */

div.tour_block .img-top {background-image: url("../images/laugh.svg"); height: 100px; background-repeat: no-repeat; background-size: contain;}
div.tour_block .img-bottom {background-image: url("../images/dot-circle.svg"); height: 100px; background-repeat: no-repeat; background-size: contain;}



/*
 * Cards have a .card-img-top  element
 *
 * You can add background images to the top of cards easily..
 *
 */

div.tour_block .card-img-top {
  height: 100px;
  background-image: url("../images/peace.svg");
  background-repeat: no-repeat;
  background-size: contain;
}

