<?php

namespace Drupal\tour_block\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Provides automated tests for the tour block module.
 */
class TourBlockControllerTest extends WebTestBase {


  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return [
      'name' => "tour TourController's controller functionality",
      'description' => 'Test Unit for module tour and controller TourController.',
      'group' => 'Other',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests tour functionality.
   */
  public function testTourController() {
    // Check that the basic functions of module tour.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via Drupal Console.');
  }

}
